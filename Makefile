DATE=$(shell date +"%F")

all:
	docker build -t pashinin/rust-debug:$(DATE) -f Dockerfile-debug .
	docker build -t pashinin/rust-release:$(DATE) -f Dockerfile-release .

build:
	docker build -t pashinin/rust-build:$(DATE) -f Dockerfile-build .

push:
	docker push pashinin/rust-debug:$(DATE)
	docker push pashinin/rust-release:$(DATE)
