# Docker images to build Rust programs

```
make
```


Then `cargo run` in any project using `debug` image:

```
docker run --rm -it \
  -v `pwd`/src:/usr/src/app/src \
  -v `pwd`/Cargo.toml:/usr/src/app/Cargo.toml \
  pashinin/rust-debug:2020-11-10 cargo run
```
