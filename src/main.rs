use actix_web::{web, App, HttpServer, middleware, Responder, get};
use serde::{Serialize};
use std::env;

#[derive(Serialize)]
struct Info<'a> {
    service: &'a str,
    version: &'a str,
    description: &'a str,
    commit_short_sha: &'a str,
}

#[get("/")]
pub async fn healthcheck() -> impl Responder {
    web::Json(Info{
        service: env!("CARGO_PKG_NAME"),
        version: env!("CARGO_PKG_VERSION"),
        description: env!("CARGO_PKG_DESCRIPTION"),
        commit_short_sha: match option_env!("CI_COMMIT_SHORT_SHA") {
            Some(sha) => sha,
            None => "",
        },
    })
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Hello, world!");
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::Logger::default())
            .service(healthcheck)
    })
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
